﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.viewmodels;

namespace MinPro334_GroupA.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiAuthController : ControllerBase
    {
        private readonly DB_SpecificationContext db;
 

        public apiAuthController(DB_SpecificationContext _db)
        {
            db = _db;
        }

        [HttpGet("CheckLogin/{email}/{password}")]
        public VMMUser CheckLogin(string email, string password)
        {
            VMMUser data = (from u in db.MUsers
                            join b in db.MBiodata on u.BiodataId equals b.Id
                            where u.IsDelete == false && u.Email == email && u.Password == password
                            select new VMMUser
                            {
                                Id = u.Id,
                                BiodataId = u.BiodataId,
                                RoleId = u.RoleId,
                                ImagePath = b.ImagePath,
                                Fullname = b.Fullname,

                            }).FirstOrDefault()!;

            return data;
        }

        [HttpGet("MenuAccess/{IdRole}")]
        public List<VMMMenuAccess> MenuAccess(int IdRole)
        {
            List<VMMMenuAccess> listMenu = (from parent in db.MMenus
                                           join mr in db.MMenuRoles on parent.Id equals mr.MenuId
                                           where parent.ParentId == 0 && mr.RoleId == IdRole && parent.IsDelete == false && mr.IsDelete == false
                                           select new VMMMenuAccess
                                           {
                                               Id = parent.Id,
                                               Name = parent.Name,
                                               BigIcon = parent.BigIcon,
                                               SmallIcon = parent.SmallIcon,
                                               ParentId = parent.ParentId,

                                               RoleId = IdRole,
                                               
                                               ListChild = (from child in db.MMenus
                                                            join mr2 in db.MMenuRoles on child.Id equals mr2.MenuId
                                                            where child.ParentId == parent.Id && child.IsDelete == false && mr2.IsDelete == false && mr2.RoleId == IdRole
                                                            select new VMMMenuAccess
                                                            {
                                                                Id = child.Id,
                                                                Name = child.Name,
                                                                BigIcon = child.BigIcon,
                                                                SmallIcon = child.SmallIcon,
                                                                ParentId = child.ParentId,

                                                                RoleId = IdRole,
                                                            }

                                               ).ToList()
                                           }).ToList()!;

            return listMenu;
        }

    }
}
