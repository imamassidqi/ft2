﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.Services;
using MinPro334_GroupA.viewmodels;

namespace MinPro334_GroupA.Controllers
{
    public class AuthController : Controller
    {
        private AuthService authService;

        VMResponse respon = new VMResponse();
        public AuthController(AuthService _authService)
        {
            authService = _authService;

        }
        public IActionResult Login()
        {
            return PartialView();
        }

        [HttpPost]
        public async Task<JsonResult> LoginSubmit(string email, string password)
        {
            VMMUser user = await authService.CheckLogin(email, password);         
            if (user != null)
            {
                respon.Message = $"Hello, Welcome to Modernize";
                HttpContext.Session.SetString("FullName", user.Fullname);
                HttpContext.Session.SetInt32("IdUser", (int)user.Id);
                HttpContext.Session.SetInt32("IdRole", (int)user.RoleId);
                HttpContext.Session.SetString("ImagePath", user.ImagePath);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"Oops, {email} not found or password is wrong, please check again";
            }
            return Json(new { dataRespon = respon });
        }
        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Index", "Home");
        }

    }
}
