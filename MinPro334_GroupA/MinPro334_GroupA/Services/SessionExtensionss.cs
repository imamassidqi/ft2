﻿using Newtonsoft.Json;

namespace MinPro334_GroupA.Services
{
    public static class SessionExtensionss
    {
        public static T GetComplexData<T>(this ISession session, string key) // T artinya bebas masukan apa aja
        {
            var data = session.GetString(key);

            if (data == null)
            {
                return default(T);
            }
            return JsonConvert.DeserializeObject<T>(data);
        }

        public static void SetComplexData(this ISession session, string key, object value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value));
        }
    }
}
