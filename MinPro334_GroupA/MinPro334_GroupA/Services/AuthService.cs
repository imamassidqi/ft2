﻿using MinPro334_GroupA.viewmodels;
using Newtonsoft.Json;

namespace MinPro334_GroupA.Services
{
    public class AuthService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
  

        public AuthService(IConfiguration _configuration)
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }

        public async Task<VMMUser> CheckLogin(string email, string password)
        {
            VMMUser data = new VMMUser();

            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiAuth/CheckLogin/{email}/{password}");
            data = JsonConvert.DeserializeObject<VMMUser>(apiResponse);

            return data;
        }

        public async Task<List<VMMMenuAccess>> MenuAccess(int IdRole)
        {
            List<VMMMenuAccess> data = new List<VMMMenuAccess>();

            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiAuth/MenuAccess/{IdRole}");
            data = JsonConvert.DeserializeObject<List<VMMMenuAccess>>(apiResponse);

            return data;
        }
    }
}
